# VIDEO FEED
# import the necessary packages
import numpy as np
from Tkinter import *
import threading
import cv2
import time
import cv2.cv as cv

# local modules
# from video import create_capture
from common import clock, draw_str
from picamera.array import PiRGBArray
from picamera import PiCamera
from pyfirmata import Arduino, util

from motor_controller import motor_control
board = Arduino('/dev/ttyACM0')
mot_right = [motor_control(board=board,
                           dir=4,
                           cdir=2,
                           pwm=3),
             motor_control(board=board,
                           dir=7,
                           cdir=6,
                           pwm=5)]
mot_left = [motor_control(board=board,
                          dir=10,
                          cdir=8,
                          pwm=9),
            motor_control(board=board,
                          dir=13,
                          cdir=12,
                          pwm=11)]

help_message = '''
USAGE: follow_color.py
'''

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
camera.rotation = 180
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)


def detect(img):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4,
                                     minSize=(30, 30),
                                     flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:, 2:] += rects[:, :2]
    return rects


def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)


def drive_motor(x1):
    if x1 > 300:
        print "right"
        mot_left[0].forward(speed=.4)
        mot_left[1].forward(speed=.4)

        mot_right[0].forward(speed=.2)
        mot_right[1].forward(speed=.2)
    elif x1 < 300:
        print "left"
        mot_left[0].forward(speed=.2)
        mot_left[1].forward(speed=.2)

        mot_right[0].forward(speed=.4)
        mot_right[1].forward(speed=.4)
    else:
        mot_left[0].forward(speed=.9)
        mot_left[1].forward(speed=.9)

        mot_right[0].forward(speed=.9)
        mot_right[1].forward(speed=.9)


class App(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.start()

    def callback(self):
        self.root.quit()

    def run(self):
        self.root = Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        label = Label(self.root, text="BGR")
        label.pack()
        self.slider_hue = Scale(self.root, from_=0, to_=255)
        self.slider_hue.pack()
        self.slider_hue.set(29)
        self.slider_saturation = Scale(self.root, from_=0, to_=255)
        self.slider_saturation.pack()
        self.slider_saturation.set(19)
        self.slider_value = Scale(self.root, from_=0, to_=255)
        self.slider_value.pack()
        self.slider_value.set(90)

# upper values
        self.slider_hue_upper = Scale(self.root, from_=0, to_=255,
                                      orient=HORIZONTAL)
        self.slider_hue_upper.pack()
        self.slider_hue_upper.set(93)
        self.slider_saturation_upper = Scale(self.root, from_=0, to_=255,
                                             orient=HORIZONTAL)
        self.slider_saturation_upper.pack()
        self.slider_saturation_upper.set(255)
        self.slider_value_upper = Scale(self.root, from_=0, to_=255,
                                        orient=HORIZONTAL)
        self.slider_value_upper.pack()
        self.slider_value_upper.set(255)

        self.slider_gray = Scale(self.root, from_=0, to_=255,
                                 orient=HORIZONTAL)
        self.slider_gray.pack()
        self.slider_gray.set(95)

        self.root.mainloop()

    def get_value(self, slider):
        if slider == "hsv_lower":
            return [self.slider_hue.get(),
                    self.slider_saturation.get(),
                    self.slider_value.get()]
        elif slider == "hsv_upper":
            return [self.slider_hue_upper.get(),
                    self.slider_saturation_upper.get(),
                    self.slider_value_upper.get()]
        elif slider == "gray":
            return self.slider_gray.get()


if __name__ == '__main__':
    import sys
    import getopt
    print help_message
    # app = App()

# capture frames from the camera
    for frame in camera.capture_continuous(rawCapture,
                                           format="bgr",
                                           use_video_port=True):
        # grab the raw NumPy array representing the image, then
        # initialize the timestamp
        # and occupied/unoccupied text
        img = frame.array
        img = cv2.medianBlur(img, 5)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        # hsv_array_lower = app.get_value(slider="hsv_lower")
        # hsv_array_upper = app.get_value(slider="hsv_upper")
        hsv_array_lower = [29, 19, 90]
        hsv_array_upper = [93, 255, 255]
        lower = np.array(hsv_array_lower)
        upper = np.array(hsv_array_upper)

        # Threshold the HSV image to get only blue colors
        # minRad = app.get_value(slider="gray")
        mask = cv2.inRange(hsv, lower, upper)

        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(img, img, mask = mask)
        gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        ret, th = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY)

        circles = cv2.HoughCircles(gray, cv.CV_HOUGH_GRADIENT, 1, 300,
                                   param1=50, param2=30, minRadius=0,
                                   maxRadius=120)
        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                # draw the outer circle
                # cv2.circle(gray, (i[0], i[1]), i[2], (0, 0, 0), 2)
                # draw the center of the circle
                # cv2.circle(gray, (i[0], i[1]), 2, (0, 0, 0), 3)
                drive_motor(x1=i[0])
                print "center points {}, {}".format(i[0], i[1])
        else:
            mot_left[0].stop()
            mot_left[1].stop()

            mot_right[0].stop()
            mot_right[1].stop()

        t = clock()
        dt = clock() - t

        # cv2.imshow('frame',img)
        # cv2.imshow('mask',mask)
        # cv2.imshow('res',res)
        cv2.imshow('gray', gray)
        key = cv2.waitKey(1) & 0xFF

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break
cv2.destroyAllWindows()
camera.close()
