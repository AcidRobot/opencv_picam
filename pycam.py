import cv2
import numpy as np
from Tkinter import *
import threading
class App(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.start()

    def callback(self):
        self.root.quit()

    def run(self):
        self.root = Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        label = Label(self.root, text="BGR")
        label.pack()
        self.slider_hue = Scale(self.root, from_=0, to_=180)
        self.slider_hue.pack()
        self.slider_hue.set(21)
        self.slider_saturation = Scale(self.root, from_=0, to_=255)
        self.slider_saturation.pack()
        self.slider_saturation.set(210)
        self.slider_value = Scale(self.root, from_=0, to_=255)
        self.slider_value.pack()
        self.slider_value.set(70)

# upper values
        self.slider_hue_upper = Scale(self.root, from_=0, to_=180,
                                      orient=HORIZONTAL)
        self.slider_hue_upper.pack()
        self.slider_hue_upper.set(45)
        self.slider_saturation_upper = Scale(self.root, from_=0, to_=255,
                                             orient=HORIZONTAL)
        self.slider_saturation_upper.pack()
        self.slider_saturation_upper.set(150)
        self.slider_value_upper = Scale(self.root, from_=0, to_=255,
                                        orient=HORIZONTAL)
        self.slider_value_upper.pack()
        self.slider_value_upper.set(255)

        self.slider_gray = Scale(self.root, from_=0, to_=255,
                                 orient=HORIZONTAL)
        self.slider_gray.pack()
        self.slider_gray.set(95)

        self.root.mainloop()

    def get_value(self, slider):
        if slider == "hsv_lower":
            return [self.slider_hue.get(),
                    self.slider_saturation.get(),
                    self.slider_value.get()]
        elif slider == "hsv_upper":
            return [self.slider_hue_upper.get(),
                    self.slider_saturation_upper.get(),
                    self.slider_value_upper.get()]
        elif slider == "gray":
            return self.slider_gray.get()

app = App()
cap = cv2.VideoCapture(0)

while(1):

    # Take each frame
    _, frame = cap.read()
    frame = cv2.medianBlur(frame, 5)
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # hsv_array_lower = app.get_value(slider="hsv_lower")
    # hsv_array_upper = app.get_value(slider="hsv_upper")
    hsv_array_lower = [29, 19, 90]
    hsv_array_upper = [93, 255, 255]
    lower = np.array(hsv_array_lower)
    upper = np.array(hsv_array_upper)

    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower, upper)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame, frame, mask = mask)
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)

    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 300,
                               param1=50, param2=30, minRadius=10, maxRadius=100)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(frame, (i[0], i[1]), i[2], (0, 0, 0), 2)
            # draw the center of the circle
            # cv2.circle(th, (i[0], i[1]), 2, (0, 0, 255), 3)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)
    cv2.imshow('gray', gray)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

cv2.destroyAllWindows()
