Open Computer Vision
==========

This uses some python libraries to follow a face or specific colour.

##Imports
numpy - this library controls some arrays that are used to store colour arrays Hue Saturation Value<br/>
Tkinter - this used in on a seperate thread to create some sliders that are used to filter out certain colors max and mins<br/>
threading - this allows us to fead live values of the sliders from Tkinter to the main program<br/>

cv2 - this is the most important <br/>
Open Computer Vision module that allows for the processing of images, and finding object from images or videos
http://opencv.org/about.html
