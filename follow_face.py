# VIDEO FEED
# import the necessary packages
import numpy as np
import cv2
import time

# local modules
# from video import create_capture
from common import clock, draw_str
from picamera.array import PiRGBArray
from picamera import PiCamera
from pyfirmata import Arduino, util

from motor_controller import motor_control
board = Arduino('/dev/ttyACM0')
mot_right = [motor_control(board=board,
                           dir=4,
                           cdir=2,
                           pwm=3),
             motor_control(board=board,
                           dir=7,
                           cdir=6,
                           pwm=5)]
mot_left = [motor_control(board=board,
                          dir=10,
                          cdir=8,
                          pwm=9),
            motor_control(board=board,
                          dir=13,
                          cdir=12,
                          pwm=11)]

help_message = '''
USAGE: face.py
'''

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
camera.rotation = 180
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)


def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4,
                                     minSize=(30, 30),
                                     flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:, 2:] += rects[:, :2]
    return rects


def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)


def drive_motor(x1, y1, x2, y2):
    area = (x2 - x1) * (y2 - y1)
    if area >= 120000:
        print "stop"
        mot_left[0].stop()
        mot_left[1].stop()

        mot_right[0].stop()
        mot_right[1].stop()
    elif area < 120000:
        print "forward"
        if x1 > (500 - x2):
            print "right"
            mot_left[0].forward(speed=.4)
            mot_left[1].forward(speed=.4)

            mot_right[0].forward(speed=.2)
            mot_right[1].forward(speed=.2)
        elif x1 < (500 - x2):
            print "left"
            mot_left[0].forward(speed=.2)
            mot_left[1].forward(speed=.2)

            mot_right[0].forward(speed=.4)
            mot_right[1].forward(speed=.4)
        else:
            mot_left[0].forward(speed=.9)
            mot_left[1].forward(speed=.9)

            mot_right[0].forward(speed=.9)
            mot_right[1].forward(speed=.9)


if __name__ == '__main__':
    import sys
    import getopt
    print help_message
    cascade_fn = '/home/pi/opencv/opencv-2.4.10/data/haarcascades/haarcascade_frontalface_alt.xml'
# Eyes
#    nested_fn = '/home/pi/opencv/opencv-2.4.10/data/haarcascades/
#                 haarcascade_eye.xml'

    cascade = cv2.CascadeClassifier(filename=cascade_fn)
#    nested = cv2.CascadeClassifier(filename=nested_fn)

# capture frames from the camera
    for frame in camera.capture_continuous(rawCapture,
                                           format="bgr",
                                           use_video_port=True):
        # grab the raw NumPy array representing the image, then
        # initialize the timestamp
        # and occupied/unoccupied text
        img = frame.array
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        # show the frame
        t = clock()
        rects = detect(gray, cascade)
        if rects != []:
            for x1, y1, x2, y2 in rects:
                print "point 1: {0},{1} \npoint 2: {2},{3}\n\n".format(x1, y1,
                                                                       x2, y2)
                drive_motor(x1=x1, y1=y1, x2=x2, y2=y2)
        else:
            mot_left[0].stop()
            mot_left[1].stop()
            mot_right[0].stop()
            mot_right[1].stop()

        dt = clock() - t
        key = cv2.waitKey(1) & 0xFF

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break
